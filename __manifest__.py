# -*- coding: utf-8 -*-
#################################################################################
# Author      : Titanius Consulting S.R.L. (<https://www.titanius.pe/>)
# Copyright(c): 2022-Present Titanius.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.titanius.pe/>
#################################################################################

{
    "name": "Attendance Face Recognition",
    "summary": """
        This module helps you to recognize the face of the employees while check in / check and 
        kiosk mode from Employee Attendance.
    """,
    "version": "15.0.1",
    "description": """
        This module helps you to recognize the face of the employees while check in / check and 
        kiosk mode from Employee Attendance.
    """,    
    "author": "Titanius Consulting S.R.L.",
    "maintainer": "Titanius Consulting S.R.L.",
    "license" :  "LGPL-3",
    "website": "https://www.titanius.pe",
    "images": ["images/attendance_webcam_image.png"],
    "category": "Human Resources",
    "depends": [
        "base",
        "hr_attendance",
    ],
    "external_dependencies":{
        'python' : ['face_recognition','cmake'],
    },
    "data": [
        "views/hr_attendance_views.xml",
        "views/hr_employee_views.xml",
        "views/res_config_settings_views.xml",
    ],
    "assets": {
        "web.assets_backend": [
            "/titan_face_recognition/static/src/css/style.css",
            "/titan_face_recognition/static/src/js/my_attendances.js",
            "/titan_face_recognition/static/src/js/kiosk_mode.js",
        ],
        "web.assets_qweb": [
            "/titan_face_recognition/static/src/xml/*.xml",
        ],
    },
    "installable": True,
    "application": True,
    "price"                 :  60.00,
    "currency"              :  "USD",
    "pre_init_hook"         :  "pre_init_check",
}