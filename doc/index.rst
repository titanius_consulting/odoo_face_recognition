Attendance Webcam Image -  Attendance Photo
===========================================
- This module helps you to capture the employees photo while check in / check and 
    kiosk mode from Employee Attendance.

Installation
========================
- Copy titan_face_recognition module to addons folder
- Install the module normally like other modules.