from odoo import models,fields
import face_recognition
import io, base64
from PIL import Image
import numpy as np
import logging
import pickle
from odoo.modules.module import get_module_path

_logger = logging.getLogger(__name__)
_cache_face_index = {}
_TOLERANCE = 0.5

class HrEmployee(models.Model):
    _inherit = "hr.employee"
    

    face_located = fields.Boolean(string="Face located in photo", default=False)

    def attendance_manual(self, next_action, entered_pin=None, match_face=False):
        res = super(HrEmployee, self.with_context(match_face=match_face)).attendance_manual(next_action, entered_pin)
        return res

    def _attendance_action_change(self):
        res = super()._attendance_action_change()
        match_face = self.env.context.get('match_face', False)
        if self.attendance_state == 'checked_in':
            res.write({
                'check_in_face': match_face,
            })
        else:
            res.write({
                'check_out_face': match_face,
            })
        return res

    def image_from_base64(self, img64, mode='RGB'):
        try:
            img = Image.open(io.BytesIO(base64.b64decode(img64)))
            if mode:
                img = img.convert(mode)
            return np.array(img) 
        except:
            return np.array([])
    


    def read_face_encode(self, id, img64):
        if '_cache_face_index' not in locals() or not _cache_face_index:
            _cache_face_index = {}
            with open(get_module_path('titan_face_recognition') + '/face_index.dat', 'rb') as file:
                _cache_face_index = pickle.load(file)
        if str(id) in _cache_face_index:
            face_encode = _cache_face_index[str(id)]
        else:
            employee_img = self.image_from_base64(img64) 
            face_encode = face_recognition.face_encodings(employee_img)[0]
            _cache_face_index[str(id)] = face_encode
        return face_encode

    def locate_face(self, img64):
        query_img = self.image_from_base64(str(img64)) 
        return self.locate_face_array(query_img)

    def locate_face_array(self, query_img):
        try:
            location = []            
            if query_img.size > 0: 
                location =  face_recognition.face_locations(query_img) 
                best={"area": 0, "id": -1}
                for i in range(len(location)):
                    area = (location[i][1] - location[i][3])*(location[i][2] - location[i][0])
                    if area > best["area"]:
                        best["area"] = area
                        best["id"] = i
                if best["id"] != -1:
                    location = [location[i]]
            return location
        except  Exception as e:
            return []
                   
    def recognize_any_face(self, img64):
        try:
            employees = self.env['hr.employee'].search([('face_located', '=', True), ('image_1920', '!=', False)])
            best_match = {"match":False, "name":"", "sim":1000} 
            query_img = self.image_from_base64(str(img64)) 
            location = self.locate_face_array(query_img)
            if len(location) == 1:                
                query_encode = face_recognition.face_encodings(query_img, location)
                for employee in employees:
                    face_encode = self.read_face_encode(employee.id, employee.image_1920) 
                    sim = face_recognition.face_distance(query_encode, face_encode)[0]
                    if sim < best_match["sim"]:
                        best_match["employee_id"] = employee.id
                        best_match["name"] = employee.name
                        best_match["sim"] = sim                
                best_match["match"] = best_match["sim"] <= _TOLERANCE
                if not best_match["match"]:
                    best_match["name"] = ""
                    best_match["employee_id"] = None
            return best_match
        except  Exception as e:   
            return {"match": False, "name":""}

    def recognize_my_face(self, img64):        
        try:
            match = False
            query_img = self.image_from_base64(str(img64)) 
            location = self.locate_face_array(query_img)
            if len(location) == 1:   
                query_encode = face_recognition.face_encodings(query_img, location)
                if self.face_located and self.image_1920:
                    face_encode = self.read_face_encode(self.id, self.image_1920)                                 
                    match = face_recognition.compare_faces(query_encode, face_encode, _TOLERANCE)
                    match = any(match) 
            return {"match": match, "location": location }
        except  Exception as e:
            _logger.exception("Error when applying face recognition: %s" % e)
            return {"match": False, "location": []}
    
    def generate_all_face_encode(self):
        employees = self.env['hr.employee'].search([('face_located', '=', True), ('image_1920', '!=', False)])
        _cache_face_index = {}
        for employee in employees:
            employee_img = self.image_from_base64(employee.image_1920) 
            face_encode = face_recognition.face_encodings(employee_img)
            if face_encode and len(face_encode)==1:   
                _cache_face_index[str(employee.id)] = face_encode[0] 
        with open(get_module_path('titan_face_recognition') + '/face_index.dat', 'wb') as file:
            pickle.dump(_cache_face_index, file)
    
    def write(self, vals):  
        if 'image_1920' in vals:
            vals['face_located'] = False
            if vals['image_1920']:
                employee_img = self.image_from_base64(vals['image_1920']) 
                face_encode = face_recognition.face_encodings(employee_img)
                if face_encode and len(face_encode)==1:                                 
                    vals['face_located'] = True   
                    _cache_face_index[str(self.id)] = face_encode  
                    with open(get_module_path('titan_face_recognition') + '/face_index.dat', 'wb') as file:
                        pickle.dump(_cache_face_index, file)       
        return super(HrEmployee, self).write(vals)