import werkzeug
from odoo import fields, models, api

class HrAttendance(models.Model):
    _inherit = "hr.attendance"
   
    check_in_face = fields.Boolean(string="Check-In Face", default = False)
    check_out_face = fields.Boolean(string="Check-Out Face", default = False)

    