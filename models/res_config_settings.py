# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, _
from odoo.modules.module import get_module_path


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    
    ## face recognition config
    allow_fr_my_attendances = fields.Boolean("Face in my attendance", default=False)
    allow_fr_kiosk = fields.Boolean("Face in kiosk mode", default=False)
               
    def get_values(self):
        """get values from the fields"""
        res = super(ResConfigSettings, self).get_values()                
        res.update(
            allow_fr_my_attendances = self.env['ir.config_parameter'].sudo().get_param('allow_fr_my_attendances'),
            allow_fr_kiosk = self.env['ir.config_parameter'].sudo().get_param('allow_fr_kiosk')
            )
        return res
    
    def set_values(self):
        """Set values in the fields"""
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('allow_fr_my_attendances', self.allow_fr_my_attendances);
        self.env['ir.config_parameter'].sudo().set_param('allow_fr_kiosk', self.allow_fr_kiosk);


    def compute_descriptors(self):
        self.env['hr.employee'].generate_all_face_encode()        
