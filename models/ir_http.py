  
from odoo import models
from odoo.http import request

class Http(models.AbstractModel):
    _inherit = "ir.http"

    def session_info(self):
        result = super(Http, self).session_info()
        if self.env.user.has_group('base.group_user'):
            result['allow_fr_my_attendances'] = self.env['ir.config_parameter'].sudo().get_param('allow_fr_my_attendances')
            result['allow_fr_kiosk'] = self.env['ir.config_parameter'].sudo().get_param('allow_fr_kiosk')
        return result