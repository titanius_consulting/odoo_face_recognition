odoo.define('attendance_face_recognition.kiosk_mode', function (require) {
    "use strict";
    
    var KioskMode = require('hr_attendance.kiosk_mode');
    var session = require("web.session");
    var core = require('web.core');
    var _t = core._t;
    var match_result = {};

    KioskMode.include({
        /**
         * @override
         */
        start: function () {            
            var self= this;            
            return Promise.resolve(this._super.apply(this, arguments)).then(function () {
                if(session.allow_fr_kiosk){
                    self.load_webcam();
                }
            })
        }, 
        /**
         * @override
         */
        destroy: function () {
            clearInterval(this._intervalWebCam);
            this._super.apply(this, arguments);
        },

        init: function (parent, action) {
            this._super.apply(this, arguments);
            this.next_action = 'hr_attendance.hr_attendance_action_kiosk_mode';
            this.employee_id = action.employee_id;
        },

        _manual_attendance: function () {
            var self = this;
           
            self._rpc({
                model: 'hr.employee',
                method: 'attendance_manual',
                args: [[self.employee_id], self.next_action, null, true],
            })
            .then(function(r) {
                if (r.action) {
                    self.do_action(r.action);
                } else if (r.warning) {
                    self.do_warn(r.warning);
                }
            });

        },

        get_video: function(){
            return this.$('#a_video').get(0);
        },
        get_canvas: function(){
            return this.$('#a_image').get(0);
        },
        get_frame: function(){
            var canvas = this.get_canvas(); 
            var img64="";
            img64 = canvas.toDataURL("image/jpeg");
            img64 = img64.replace(/^data:image\/(png|jpg|jpeg|webp);base64,/, "");
            return img64;
        },
        load_webcam: function(){            
            var self = this;
            if (self.get_video()){
                navigator.getUserMedia = navigator.getUserMedia 
                    || navigator.webkitGetUserMedia 
                    || navigator.mozGetUserMedia
                    || navigator.msGetUserMedia;
                if (navigator.getUserMedia) {
                    if (self.$('#a_webcam')){
                        self.$('#a_webcam').removeClass('d-none');
                        var media = navigator.getUserMedia(
                            { 'video': {width: 160, height: 160} },
                            function(stream) {                                             
                                self.play_video(stream);                           
                            },
                            function(err) {
                                console.log("on loaded metadata");
                            }
                        );
                    }
                }else if(navigator.mediaDevices.getUserMedia){
                    if (self.$('#a_webcam')){
                        self.$('#a_webcam').removeClass('d-none');
                        var media = navigator.mediaDevices.getUserMedia({
                            'video': true,
                            'audio': false
                        })
                        .then(function(stream) {
                            self.play_video(stream); 
                        })
                        .catch(function(err){
                            console.log(err.name + ": " + err.message);
                        });
                    }
                }else{
                    self.displayNotification({ message: _t('Please Update or Use Different Browser'), type: 'danger' });
                    return;
                }
            }else{
                setTimeout( function() { 
                    self.loadWebcam();
                }, 500);
            }            
        },
        play_video: function(stream){
            var self= this; 
            var video = self.get_video();
            var canvas = self.get_canvas(); 
            video.srcObject = stream; 
            video.muted = true;   
            const requestFrame = (cb) => {
                if(video.requestVideoFrameCallback) {
                    return video.requestVideoFrameCallback(cb);
                }
                return requestAnimationFrame(cb);
            };
            video.play().then(() => {
                canvas.width = $(video).width();
                canvas.height = $(video).height();   
                const ctx = canvas.getContext('2d');
                ctx.font = "12px Arial";
                ctx.textAlign = "center";

                self._intervalWebCam = setInterval(() => requestFrame(draw), 250);
                self.locate_face();
                
                function draw() {                                     
                    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

                    if(match_result.location &&  match_result.location.length > 0)
                    {
                        let rect = match_result.location[0]
                        ctx.beginPath();  
                        if(match_result.match)
                        {
                            //draw location rectangle
                            ctx.strokeStyle = '#00ff00';
                            ctx.rect(rect[3], rect[0], rect[1] - rect[3], rect[2] - rect[0]);
                            //draw name
                            ctx.fillStyle = "green";
                            ctx.fillRect(0, canvas.height - 20, canvas.width, 20);
                            ctx.fillStyle = "white";
                            ctx.fillText(match_result.name, canvas.width/2, canvas.height-5);
                        }
                        else 
                        {
                            ctx.strokeStyle = '#ff0000';
                            ctx.rect(rect[3], rect[0], rect[1] - rect[3], rect[2] - rect[0]);
                        }
                        ctx.stroke();                        
                    }
                }                
            });      
        }, 
        
        stop_video: function(){
            var video = self.$('#a_video').get(0);
            if(video.srcObject){
                const tracks = video.srcObject.getTracks();
                tracks.forEach(track => track.stop());
            }
        },
        locate_face:  function () {
            var self = this;
            var img64 = self.get_frame();
            this._rpc({
                model: 'hr.employee',
                method: 'locate_face',
                args: [[self.employee_id], [img64]],
            })
            .then(function(result) {
                match_result.location = result;  

                if(match_result.location &&  match_result.location.length > 0)
                    self.recognize_any_face(img64);
                else {
                    setTimeout(() => {
                        self.locate_face();
                    }, 500);    
                }            
            });
        },        

        recognize_any_face:  function (img64) {
            var self = this;
            this._rpc({
                model: 'hr.employee',
                method: 'recognize_any_face',
                args: [[self.employee_id], [img64]],
            })
            .then(function(result) {
                match_result.match = result.match == 'True';
                match_result.name = result.name; 
                self.employee_id = result.employee_id; 
                
                if(match_result.match){
                    self._manual_attendance();                   
                } 
                else{
                    setTimeout(() => {
                        self.locate_face();
                    }, 500);    
                }
            });
        },              
    });
})
