odoo.define('attendance_face_recognition.my_attendances', function (require) {
    "use strict";
    
    var MyAttendances = require('hr_attendance.my_attendances');
    var session = require("web.session");
    var core = require('web.core');
    var _t = core._t;
    var match_result = [];
    var counter = 0;

    MyAttendances.include({
        events: _.extend(MyAttendances.prototype.events, {
            "click .o_hr_attendance_sign_in_out_icon":  _.debounce(function() {
                var self = this;
                self.check_face_recognition();
            }, 200, true),
        }),
                     
        check_face_recognition: function(){
            var self= this; 
            if(session.allow_fr_my_attendances){
                self.$('a.o_hr_attendance_sign_in_out_icon').addClass('d-none');
                self.$('h3.text-muted').html(_t('Please focus your face on the camera'));
                self.load_webcam();
            }
            else{
                self.update_attendance();
            }
        },

        _manual_attendance: function (match_face) {
            var self = this;
            this._rpc({
                model: 'hr.employee',
                method: 'attendance_manual',
                args: [[self.employee.id], 'hr_attendance.hr_attendance_action_my_attendances', null, match_face],
            })
            .then(function(result) {
                if (result.action) {
                    self.do_action(result.action);
                } else if (result.warning) {
                    self.do_warn(result.warning);
                }
            });
        },  
        
        get_video: function(){
            return this.$('#a_video').get(0);
        },
        get_canvas: function(){
            return this.$('#a_image').get(0);
        },
        get_frame: function(){
            var canvas = this.get_canvas(); 
            var img64="";
            img64 = canvas.toDataURL("image/jpeg");
            img64 = img64.replace(/^data:image\/(png|jpg|jpeg|webp);base64,/, "");
            return img64;
        },
        load_webcam: function(){
            var self= this; 
            if(self.get_video()){
                navigator.getUserMedia = navigator.getUserMedia 
                    || navigator.webkitGetUserMedia 
                    || navigator.mozGetUserMedia
                    || navigator.msGetUserMedia;
                if (navigator.getUserMedia) {
                    if (self.$('#a_webcam')){
                        self.$('#a_webcam').removeClass('d-none');
                        var media = navigator.getUserMedia(
                            { 
                                'video': {width: 320, height: 320} 
                            },
                            function(stream) {                                             
                                self.play_video(stream);                                                                                           
                            },
                            function(err) {
                                console.log("on loaded metadata");
                            }
                        );
                    }   
                }else if(navigator.mediaDevices.getUserMedia){                                             
                    if (self.$('#a_webcam')){
                        self.$('#a_webcam').removeClass('d-none');
                        var media = navigator.mediaDevices.getUserMedia({
                            'video': true,
                            'audio': false
                        })
                        .then(function(stream) {                                                           
                            self.play_video(stream);  
                        })
                        .catch(function(err){
                            console.log(err.name + ": " + err.message);
                        });
                    }
                }else{
                    self.displayNotification({message: _t('Please Update or Use Different Browser'), type: 'danger' });
                }
            }
            else {
                setTimeout( function() { 
                    self.load_webcam();
                }, 500);
            }
        },
        play_video: function(stream){
            var self= this; 
            var video = self.get_video();
            var canvas = self.get_canvas(); 
            video.srcObject = stream; 
            video.muted = true;   
            const requestFrame = (cb) => {
                if(video.requestVideoFrameCallback) {
                    return video.requestVideoFrameCallback(cb);
                }
                return requestAnimationFrame(cb);
            };
            video.play().then(() => {
                canvas.width = $(video).width();
                canvas.height = $(video).height();   
                const ctx = canvas.getContext('2d');

                let timerDrawId = setInterval(() => requestFrame(draw), 250);
                self.recognize_my_face(timerDrawId);
                counter = 0;

                function draw() {                                     
                    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
                    if(match_result.location &&  match_result.location.length > 0)
                    {
                        for(let i =0 ;i < match_result.location.length; i++){
                            let rect = match_result.location[i]
                            ctx.beginPath();  
                            ctx.strokeStyle = '#ff0000';
                            ctx.rect(rect[3], rect[0], rect[1] - rect[3], rect[2] - rect[0]);
                            ctx.stroke();                        
                        }
                    }
                    counter++;
                }                
            });      
        }, 
        stop_video: function(){
            var video = this.get_video();
            const tracks = video.srcObject.getTracks();
            tracks.forEach(track => track.stop());
        },
        recognize_my_face:  function (timerId) {
            var self = this;
            var img64 = self.get_frame();
            this._rpc({
                model: 'hr.employee',
                method: 'recognize_my_face',
                args: [[self.employee.id], [img64]],
            })
            .then(function(result) {
                match_result = result;
                if(match_result.match){                    
                    clearInterval(timerId);
                    self._manual_attendance(match_result.match);
                    self.stop_video();
                } 
                //after 5 seconds close the webcam
                else if(counter >= 20){
                    clearInterval(timerId);
                    self.stop_video();
                    self.$('h3.text-muted').html(_t('<spam class="text-center text-danger  blinking">Face is unrecognizable</spam>'));
                    counter = 0;
                }
                else {
                    self.recognize_my_face(timerId);
                }
            });
        },        
    });
});
